# [labs.javadev.org](https://labs.javadev.org) source codes

<br/>

### Run labs.javadev.org on localhost

    # vi /etc/systemd/system/labs.javadev.org.service

Insert code from labs.javadev.org.service

    # systemctl enable labs.javadev.org.service
    # systemctl start labs.javadev.org.service
    # systemctl status labs.javadev.org.service

http://localhost:4026
