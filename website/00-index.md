---
layout: page
title: Java Labs
permalink: /
---


# Java Labs


<br/>

### GitHub Blocked my Free Accounts without any Warnings and Notifications

You may check: github.com/javadev-org is not accessible now.

For that reason i have to move all projects to bitbucket.org.

Another alternative - gitlab.com

May be later I'll up my own gitlab service and will store all project on place where no one can block my codes.


**Sorry for not working links on my github projects.**

Direct link on <a href="https://bitbucket.org/javadev-org/labs.javadev.org" target="_blank">labs.javadev.org</a> source codes.



<br/>

## About this website:

On this website, I'm trying to investigate any materials about java development to improve my skills.

Materials marked with green I've seen on Internet for downloading from trackers, file shares etc.

If you can send video courses not marked green to us or can add courses not in our list, we will appreciate it.


<br/>


**If you will investigate any materials, you can add your experience or you can add link to your public repo**



My contact: <br/>
<img src="http://img.fotografii.org/a3333333mail.gif" alt="Marley">


<a href="https://travis-ci.org/javadev-org/labs.javadev.org" rel="nofollow"><img src="https://travis-ci.org/javadev-org/labs.javadev.org.svg?branch=gh-pages" alt="javalabs build"></a>


<br/>

### Please share interesting code samples on java with us:

If you find interesting project on github, bitbucket, book, video course etc. on java, please send a link to us.  
For example, this repository is very <a href="https://github.com/iluwatar/java-design-patterns" rel="nofollow">helpful</a> i think.


<!-- <br/>


<strong>[Link on Java Tutorials]:</strong><br/>

<a href="http://winterbe.com/tutorials/" rel="nofollow">Threads, Streams, Concurrency etc.</a> -->



<br/><br/>

<strong>[Lab 7]:</strong><br/>

### [YouTube, JavaBrains] Java 8 Lambda Basics

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/videoseries?list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

<a href="/labs/lab-07/">[here]</a>




<br/><br/>
<strong>[YouTube][Arun Gupta] Developing Web Applications with WildFly 8</strong>
<br/><br/>

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/5MhqcVFVJ2s" frameborder="0" allowfullscreen></iframe>

</div>




<br/><br/>

<strong>[Lab 6]:</strong><br/>

### [YouTube] Selenium Grid Understanding and Configuring

<div align="center">

<iframe width="853" height="480" src="https://www.youtube.com/embed/g1IYmdmsCkQ?list=PL6tu16kXT9Po4YMQz_uEd5FN4V3UyAZi6" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

<a href="/labs/lab-06/">[here]</a>


<br/><br/>

<strong>[Lab 5]:</strong><br/>

### [Spring] [Udemy] Building An E-Commerce Store Using Java Spring Framework

<div align="center">

<iframe width="853" height="480" src="https://www.youtube.com/embed/BWgwWIbH3PI" frameborder="0" allowfullscreen></iframe>

</div>


<a href="/labs/lab-05/">[here]</a>



<br/><br/>

<strong>[Lab 4]:</strong><br/>

### [Android] Android App Development Fundamentals I and II (Paused)

<a href="/labs/informit/2014/android_app_development_fundamentals/">[Informit] Paul Deitel: Android App Development Fundamentals I and II</a>



<br/><br/>

<strong>[Lab 3]:</strong><br/>

### [Android] Fundamentals (Paused)

<a href="/labs/udacity/2014/developing_android_apps/">[Udacity] Developing Android Apps (Android Fundamentals)</a>



<br/><br/>
<strong>[Lab 2]:</strong><br/>

### [Book] Java EE 7 Development with WildFly (Paused):


I'm planning to start reading book
<a href="/labs/books/packtpub/2014/java-ee-7-development-with-wildfly/">Java EE 7 Development with WildFly</a>. I want try to investigate code examples. If someone will want to discuss or participate, welcome.



<br/><br/>
<strong>[Lab 1]:</strong><br/>

### [YouTube] JBoss AS and ActiveMQ (Completed):

<a href="/labs/youtube/ternovich/2011/jboss-as-and-activemq/">[Ternovich] JBoss AS and ActiveMQ</a>

<br/><br/>
<br/><br/>
