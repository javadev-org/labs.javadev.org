---
layout: page
title: Some Videos About Java Development From YouTube
permalink: /youtube-videos/
---

# Some Videos About Java Development From YouTube

<div style="padding:10px; border:thin solid black;">

<h3>CS 251 (2015): Intermediate Software Design</h3>

    <div align="center">

    <iframe width="560" height="315" src="//www.youtube.com/embed/videoseries?list=PLZ9NgFYEMxp7lylj-XC8h1kjatOjbh9ne" frameborder="0" allowfullscreen></iframe>

    </div>

<br/><br/>

<hr>
<br/><br/>

<h3>CS 282 (2014): Concurrent Java Network Programming in Android</h3>

    <div align="center">

    <iframe width="560" height="315" src="//www.youtube.com/embed/videoseries?list=PLZ9NgFYEMxp4KSJPUyaQCj7x--NQ6kvcX" frameborder="0" allowfullscreen></iframe>

    </div>

</div>

<br/><br/>

<hr>
<br/><br/>

<div style="padding:10px; border:thin solid black;">
<h3>JSF & PrimeFaces & Spring tutorial</h3>

    <div align="center">

    <iframe width="560" height="315" src="//www.youtube.com/embed/3RFhjzNdbfA" frameborder="0" allowfullscreen></iframe>

    </div>

<br/><br/>

http://www.youtube.com/playlist?list=PLmcxdcWPhFqPq23QswZYbKvhs2Eo6XyJA

</div>

<br/><br/>

<hr>
<br/><br/>

<div style="padding:10px; border:thin solid black;">
<h3>Java EE (J2EE) Tutorial for beginners - Real World App</h3>

Learn how to create Real World enterprise web application from scratch using modern Java frameworks in one integration: Spring Framework + Spring Webflow + Spring Security + JPA (Hibernate) + JSF 2.0 (PrimeFaces) + Apache Maven 2 + Apache Tomcat + Oracle RDBMS + Eclipse IDE.

    <div align="center">

    <iframe width="560" height="315" src="//www.youtube.com/embed/videoseries?list=PLHqN89yRGMyAcwWcSWk59_S_-BQVn3Rrb" frameborder="0" allowfullscreen></iframe>

    </div>

<br/><br/>
Later we will integrate a SOAP Web Services and XML JAXB marshaling/unmarshaling into application that we will build during this tutorial.

<br/>
<br/>
https://www.youtube.com/playlist?list=PLHqN89yRGMyAcwWcSWk59_S_-BQVn3Rrb

</div>

<br/><br/>

<hr>
<br/><br/>

<div style="padding:10px; border:thin solid black;">
<h3>Java EE 7 Samples Deep Dive - Part 1</h3>

    <div align="center">

        <iframe width="560" height="315" src="http://www.youtube.com/embed/J19Cjq-U7C4" frameborder="0" allowfullscreen></iframe>

    </div>

<h3>Java EE 7 Samples Deep Dive - Part 2</h3>

    <div align="center">

    <iframe width="560" height="315" src="http://www.youtube.com/embed/7K-ZIq6pdi0" frameborder="0" allowfullscreen></iframe>

    </div>

</div>

<br/><br/>

<hr>
<br/><br/>

<div style="padding:10px; border:thin solid black;">
<h3>Getting Started with Tdd in Java using Eclipse</h3>

    <div align="center">

    <iframe src="http://player.vimeo.com/video/10569751" width="500" height="375" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>

    </div>

</div>

<br/><br/>

<hr>
<br/><br/>

<div style="padding:10px; border:thin solid black;">
<h3>M101J: MongoDB for Java Developers [May 13, 2013 - July 1, 2013]</h3>

Learn everything you need to know to get started building a MongoDB-based app.

This course will go over basic installation, JSON, schema design, querying, insertion of data, indexing and working with language drivers.
In the course, you will build a blogging platform, backed by MongoDB. Our code examples will be in Java.

    <div align="center">

    <img src="/files/img/labs/mongo.jpg" alt="java and mongoDB Training">

    <br/><br/>

    <a href="magnet:?xt=urn:btih:27c5157758cf0cf1f03115895fe5a6a539f86228&dn=%5BMongoDB%20University%5D%20MongoDB%20for%20Java%20Developers%20%5B2013,%20ENG%5D&tr=http%3A%2F%2Fbt.rutracker.org%2Fann%3Fuk%3D6WROVr5o1y&tr=http%3A%2F%2Fretracker.local%2Fannounce">Magnetlink</a>


    </div>

</div>
