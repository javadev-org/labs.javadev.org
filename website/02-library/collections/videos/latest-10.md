---
layout: page
title: Latest 10 added courses about java development
permalink: /library/videos/latest10/
---


# Latest 10 added courses about java development

<br/>

### Latest 1 added free courses:

<ul>

	<li style="color:green"><strong>[QA Automan] Selenium & TestNG </strong> [ENG, ????, 2014] (<a href="/library/qa_automan/java/free/"> Link on Description</a>)</li>

</ul>

<br/>

### Latest 10 added paid courses:

<ul>

    <li style="color:green">
        <strong>[LiveLessons] Modern Java Collections [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Packt] Hands-On Enterprise Application Development with Java 9 [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Eduonix] Projects in Enterprise Java [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Effective Automated Testing with Spring [2018, ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Spring Cloud Fundamentals [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Java Microservices with Spring Cloud Developing Services [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Java Microservices with Spring Cloud Coordinating Services [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Packt] Spring Design Patterns and Best Practices [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Udemy] Design Patterns in Java – Concepts & Hands On Projects [ENG]</strong>
    </li>
    
    <li style="color:green">
        <strong>[Addison-Wesley Professional] Core Java 9/10 for the Impatient [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Addison-Wesley Professional] Core Java 9, Second Edition [ENG]</strong>
    </li>
    
    <li style="color:green">
        <strong>[Addison-Wesley Professional] Core Java 9 Advanced, Second Edition [April 2018, ENG]</strong>
    </li>



</ul>

<br/>

### Latest 2 added paid android courses:

<ul>

    <li style="color:green"><strong>[Pluralsight] Android Animationsg</strong> [ENG, 2h 13m, 23 May 2015] (<a href="http://www.pluralsight.com/courses/android-animations"> Course Description</a>)</li>


    <li style="color:green"><strong>[Lynda] Distributing Android Apps</strong> [ENG, 1h 38m, Feb 11, 2014] (<a href="http://www.lynda.com/Android-tutorials/Distributing-Android-Apps/143101-2.html"> Course Description</a>)</li>

</ul>



<br/>

### Additional:

<ul>

	<li style="color:green"><strong>[O'Reilly] Software Architecture Fundamentals I [ENG, 6 hours 57 minutes, March 2014] <a href="http://shop.oreilly.com/product/110000195.do"> Course Description</a></strong></li>

	<li style="color:green"><strong>[O'Reilly] Software Architecture Fundamentals II [ENG, 5 hours 57 minutes, April 2014] <a href="http://shop.oreilly.com/product/110000195.do"> Course Description</a></strong></li>

	<li style="color:green"><strong>[O'Reilly] Software Architecture Fundamentals Part III [ENG, ????, March 2015] <a href="http://shop.oreilly.com/product/0636920039464.do"> Course Description</a></strong></li>

	<li style="color:green"><strong>[O'Reilly] Software Architecture Fundamentals Part IV [ENG, ????, March 2015] <a href="http://shop.oreilly.com/product/0636920040309.do"> Course Description</a></strong></li>

</ul>

<br/>

### Clean Code:

<ul>
	<li style="color:green"><strong>Clean Coders - Clean Code Video Series - Episode 1-30 Completed [Full HD] (2015) </strong></li>
</ul>

<br/>

### UML:

<ul>

	<li style="color:green"><strong>Infiniteskills - UML Fundamentals Training [ENG, 6 hours, 2015] <a href="http://www.infiniteskills.com/training/uml-fundamentals.html"> Course Description</a></strong></li>

</ul>
