---
layout: page
title: Lynda Java Videos
permalink: /library/lynda/java/
---

# Lynda Java Videos

<br/><br/>

<strong>2017</strong>

<ul>
	<li style="color:green"><strong>[Lynda.com / Peggy Fisher] Java Database Access with Hibernate [2017, ENG] </strong><a href="https://www.lynda.com/Java-tutorials/Java-Database-Access-Hibernate/534635-2.html" rel="nofollow">Course Description</a></li>

</ul>


<ul>
	<li style="color:green"><strong>[Lynda.com / Paul Trani] Java for Data Scientists Essential Training [2017, ENG] </strong><a href="https://www.lynda.com/Java-tutorials/Java-Data-Scientists-Essential-Training/518162-2.html" rel="nofollow">Course Description</a></li>

</ul>

<br/><br/>

<strong>2015</strong>

<ul>
	<li style="color:green"><strong>[Lynda] Google App Engine Essential Training [ENG, 3h 26m, Apr 30, 2015] </strong><a href="http://www.lynda.com/Developer-Cloud-Computing-tutorials/Google-App-Engine-Essential-Training/194134-2.html" rel="nofollow">Course Description</a></li>
	<li style="color:green"><strong>[Lynda] Up and Running with Java</strong></li>
</ul>



<br/><br/>

<strong>2014</strong>

<ul>
	<li style="color:green"><strong>Using Java to Program Google App Engine</strong></li>
	<li style="color:green"><strong>Java EE Essentials: Enterprise JavaBeans</strong> (<a href="https://bitbucket.org/javalabs/java-ee-essentials-enterprise-javabeans" rel="nofollow">src</a>)</li>
	<li style="color:green"><strong>Java EE Essentials: Servlets and JavaServer Faces</strong></li>
	<li style="color:green"><strong>Java SE 8 New Features</strong></li>
	<li style="color:green"><strong>Building Web Services with Java EE</strong></li>
</ul>



<br/><br/>

<strong>2013</strong>


<ul>
	<li style="color:green"><strong>Foundations of Programming: Design Patterns (+RUS)</strong></li>
	<li style="color:green"><strong>XML Integration with Java</strong></li>
</ul>


<br/><br/>

<strong>2012</strong>

<ul>
	<li style="color:green"><strong>Java Database Integration with JDBC</strong></li>
	<li style="color:green"><strong>Java Advanced Training</strong></li>
	<li style="color:green"><strong>Foundations of Programming: Object-Oriented Design</strong></li>
	<li style="color:green"><strong>Up and Running with Java Applications</strong></li>
</ul>


<br/><br/>

<strong>2011</strong>

<ul>
	<li style="color:green"><strong>Java Essential Training</strong></li>
</ul>


### Android

<strong>2014</strong>

<ul>
	<li style="color:green"><strong>[Lynda] Distributing Android Apps</strong> [ENG, 1h 38m, Feb 11, 2014] (<a href="http://www.lynda.com/Android-tutorials/Distributing-Android-Apps/143101-2.html" rel="nofollow"> Course Description</a>)</li>
</ul>
