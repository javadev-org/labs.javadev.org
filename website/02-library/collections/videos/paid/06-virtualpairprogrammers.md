---
layout: page
title: virtualpairprogrammers
permalink: /library/virtualpairprogrammers/java/
---


# virtualpairprogrammers


<ul>

    <li style="color:green">
        <strong>[virtualpairprogrammers.com] Hadoop for Java Developers [2014, ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[virtualpairprogrammers.com] Spring Security: Module 2 - OAuth2 and REST [2015, ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[virtualpairprogrammers.com] Spring JavaConfig [2015, ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[virtualpairprogrammers.com] Spring Security: Module 1 - Core Concepts [2014, ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[virtualpairprogrammers.com] Java Web Development [2009, ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[virtualpairprogrammers.com] Java Advanced Topics [2015, ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[virtualpairprogrammers.com] Java Fundamentals [2015, ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[virtualpairprogrammers.com] JavaEE [2014, ENG]</strong>
    </li>
    <li style="color:green">
        <strong>[virtualpairprogrammers.com] JavaEE and WildFly Module 1 [2015, ENG]</strong>
    </li>
    <li style="color:green">
        <strong>[virtualpairprogrammers.com] JavaEE Messaging with JMS and MDB [ENG]</strong>
    </li>
    <li style="color:green">
        <strong>[virtualpairprogrammers.com] Java Build Tools [2014, ENG]</strong>
    </li>
</ul>
