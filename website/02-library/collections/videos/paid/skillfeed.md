---
layout: page
title: Skillfeed Java Videos
permalink: /library/skillfeed/java/
---

# Skillfeed Java Videos

<strong>2015</strong>

<ul>
<li><strong>[Skillfeed] Java Tutorial For Absolute Beginners [ENG, 5 hr 24 min, 2015]</strong> <a href="https://www.skillfeed.com/courses/10302-java-tutorial-for-absolute-beginners">(Course Description)</a></li>
<li><strong>[Skillfeed] Java Swing Essentials - GUI programming in Java made easy!! [ENG, 2 hr 19 min, 2015]</strong> <a href="https://www.skillfeed.com/courses/9496-java-swing-essentials-gui-programming-in-java-made-easy">(Course Description)</a></li>
<li><strong>[Skillfeed] Java for beginners : A very easy course on Java [ENG, 2 hr 36 min, 2015]</strong> <a href="https://www.skillfeed.com/courses/10877-java-for-beginners-a-very-easy-course-on-java">(Course Description)</a></li>
<li><strong>[Skillfeed] Automation Testing using Selenium Webdriver and Java [ENG, 3 hr 22 min, 2015]</strong> <a href="https://www.skillfeed.com/courses/10275-automation-testing-using-selenium-webdriver-and-java">(Course Description)</a></li>
<li><strong>[Skillfeed] Become a professional Java developer from scratch [ENG, 5 hr 4 min, 2015]</strong> <a href="https://www.skillfeed.com/courses/10489-become-a-professional-java-developer-from-scratch">(Course Description)</a></li>
<li><strong>[Skillfeed] Java Tutorial For Beginners [ENG, 9 hr 41 min, 2015]</strong> <a href="https://www.skillfeed.com/courses/10156-java-tutorial-for-beginners">(Course Description)</a></li>
<li><strong>[Skillfeed] EJB 3.0 in Simple Steps, Do it Right Do it Fast [ENG, 2 hr 31 min, 2015]</strong> <a href="https://www.skillfeed.com/courses/9890-ejb-3-0-in-simple-steps-do-it-right-do-it-fast">(Course Description)</a></li>
<li><strong>[Skillfeed] Java Memory Management (Heap, stack space and string interning) [ENG, 36 min, 2015]</strong> <a href="https://www.skillfeed.com/courses/9266-java-memory-management-heap-stack-space-and-string-interning">(Course Description)</a></li>
<li><strong>[Skillfeed] WebServices/REST API Testing with SoapUI [ENG, 13 hr 42 min, 2015]</strong> <a href="https://www.skillfeed.com/courses/9095-webservices-rest-api-testing-with-soapui">(Course Description)</a></li>

</ul>


<strong>2014</strong>

<ul>

<li><strong>[Skillfeed] Design Patterns Through Java [ENG, 4 hr 21 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/8802-design-patterns-through-java">(Course Description)</a></li>
<li><strong>[Skillfeed] Java Tutorial for Complete Beginners [ENG, 15 hr 47 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/4270-java-tutorial-for-complete-beginners">(Course Description)</a></li>
<li><strong>[Skillfeed] Java Applet Tutorial For Beginner [ENG, 54 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/8239-java-applet-tutorial-for-beginner">(Course Description)</a></li>
<li><strong>[Skillfeed] Learn Spring Security in Plain English - Easily Add Login/Passwords to your Java Web Applications [ENG, 2 hr 38 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/8103-learn-spring-security-in-plain-english-easily-add-login-passwords-to-your-java-web-applications">(Course Description)</a></li>

<li style="color:green"><strong>[Skillfeed] Java EE Development [ENG, 6 hr 20 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/6973-java-ee-development">(Course Description)</a></li>

<li><strong>[Skillfeed] Java - Java Programming for Beginners! [ENG, 6 hr 8 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/6951-java-java-programming-for-beginners">(Course Description)</a></li>

<li><strong>[Skillfeed] Introduction to Unit Testing in Java with J-Unit 4 [ENG, 1 hr 25 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/6215-introduction-to-unit-testing-in-java-with-j-unit-4">(Course Description)</a></li>

<li><strong>[Skillfeed] Java Game Development Tutorial - Part II [ENG, 2 hr 7 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/5991-java-game-development-tutorial-part-ii">(Course Description)</a></li>
<li><strong>[Skillfeed] Java Game Development Tutorial - Part I [ENG, 2 hr 14 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/5941-java-game-development-tutorial-part-i">(Course Description)</a></li>
<li style="color:green"><strong>[Skillfeed] Intermediate & Advanced Java Programming [ENG, 3 hr 37 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/2105-intermediate-advanced-java-programming">(Course Description)</a></li>
<li><strong>[Skillfeed] Learn to Program with Java: Academic Edition [ENG, 20 hr 33 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/4693-learn-to-program-with-java-academic-edition">(Course Description)</a></li>
<li><strong>[Skillfeed] Advance Java Programming [ENG, 1 hr 27 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/5781-advance-java-programming">(Course Description)</a></li>
<li><strong>[Skillfeed] Learn Java with NetBeans! [ENG, 7 hr 28 min, 2014]</strong> <a href="https://www.skillfeed.com/courses/5608-learn-java-with-netbeans">(Course Description)</a></li>
</ul>
