---
layout: page
title: Pluralsight Java Videos
permalink: /library/pluralsight/java/
---

# Pluralsight Java Videos

<br/>


<ul>

    <li style="color:green">
        <strong>[Pluralsight] Effective Automated Testing with Spring [2018, ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Spring Cloud Fundamentals [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Java Microservices with Spring Cloud Developing Services [ENG]</strong>
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Java Microservices with Spring Cloud Coordinating Services [ENG]</strong>
    </li>

</ul>



<strong>2017</strong>

<br/>

<ul>

    <li style="color:green">
        <strong>[Pluralsight] Java EE: Programming Servlets [ENG, 2017]</strong> (<a href="https://www.pluralsight.com/courses/java-ee-programming-servlets" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Java EE: Getting Started with JavaServer Faces [ENG, 2017]</strong> (<a href="https://www.pluralsight.com/courses/javaserver-faces-getting-started-java-ee" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Java EE: Java Server Pages [ENG, 2017]</strong> (<a href="https://www.pluralsight.com/courses/java-ee-java-server-pages" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Advanced Java Concurrent Patterns [ENG, 2017]</strong> (<a href="https://www.pluralsight.com/courses/java-concurrent-patterns-advanced" rel="nofollow">Course Description</a>)
    </li>
</ul>


<br/>

<strong>2016</strong>

<br/>

<ul>

    <li style="color:green">
        <strong>[Pluralsight] Play by Play: Docker for Java Developers with Arun Gupta and Michael Hoffman [ENG, 2016]</strong> (<a href="https://www.pluralsight.com/courses/play-by-play-docker-java-developers-arun-gupta-michael-hoffman" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green">
        <strong>[Pluralsight, Dustin Schultz] Spring Boot: Efficient Development, Configuration, and Deployment [ENG, 2h 24m, 16 Jun 2016]</strong> (<a href="https://www.pluralsight.com/courses/spring-boot-efficient-development-configuration-deployment" rel="nofollow">Course Description</a>)
    </li>


    <li style="color:green">
        <strong>[Pluralsight, Kevin Jones] Architecting Web Applications with Spring [ENG, 1h 57m, 24 May 2016]</strong> (<a href="https://www.pluralsight.com/courses/spring-boot-first-application" rel="nofollow">Course Description</a>)
    </li>


    <li style="color:green">
        <strong>[Pluralsight, Dan Bunker] Creating Your First Spring Boot Application [ENG, 2h 35m, 17 Mar 2016]</strong> (<a href="https://www.pluralsight.com/courses/spring-boot-first-application" rel="nofollow">Course Description</a>)
    </li>
</ul>


<br/><br/>


<strong>2015</strong>


<ul>

    <li style="color:green">
        <strong>[Pluralsight] Java Fundamentals: Generics [2015, ENG]</strong> (<a href="https://www.pluralsight.com/courses/java-generics" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green">
        <strong>[Pluralsight] From Collections to Streams in Java 8 Using Lambda Expressions [ENG, 4:01]</strong> (<a href="https://www.pluralsight.com/courses/java-8-lambda-expressions-collections-streams" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Streams, Collectors, and Optionals for Data Processing in Java 8 [ENG, 4:29]</strong> (<a href="https://www.pluralsight.com/courses/java-8-data-processing-streams-collectors-optionals" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Getting Started with Spring Data REST [ENG, 2h 45m, 23 Oct 2015]</strong> (<a href="http://www.pluralsight.com/courses/spring-data-rest-getting-started" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green">
        <strong>[Pluralsight] From Collections to Streams in Java 8 Using Lambda Expressions [ENG, 4h 1m, 16 Sep 2015]</strong> (<a href="http://www.pluralsight.com/courses/java-8-lambda-expressions-collections-streams" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green">
        <strong>[Pluralsight] Design Patterns in Java: Structural [ENG, 2h 42m, 20 Aug 2015]</strong> (<a href="http://www.pluralsight.com/courses/design-patterns-java-structural" rel="nofollow">Course Description</a>)
    </li>

    <li style="color:green"><strong>[Pluralsight] Understanding the Java Virtual Machine: Security</strong> [ENG, 1h 21m, 18 Jul 2015] (<a href="http://www.pluralsight.com/courses/understanding-java-vm-security" rel="nofollow">Course Description</a>)</li>

    <li style="color:green"><strong>[Pluralsight] Understanding the Java Virtual Machine: Memory Management </strong> [ENG, 1h 36m, 04 Jun 2015] (<a href="http://www.pluralsight.com/courses/understanding-java-vm-memory-management" rel="nofollow"> Course Description</a>)</li>

    <li style="color:green"><strong>[Pluralsight] Introduction to Testing in Java </strong> [ENG, 3h 54m, 11 May 2015] (<a href="http://www.pluralsight.com/courses/java-testing-introduction" rel="nofollow"> Course Description</a>)</li>

    <li style="color:green"><strong>[Pluralsight] Getting Started With Java EE on WebSphere® Application Server</strong> [ENG, 2h 54m, 07 May 2015] (<a href="http://www.pluralsight.com/courses/java-ee-websphere-application-server-getting-started" rel="nofollow"> Course Description</a>)</li>

    <li style="color:green"><strong>[Pluralsight] Design Patterns in Java: Creational</strong> [ENG, 1h 50m, 07 Apr 2015] (<a href="http://www.pluralsight.com/courses/design-patterns-java-creational" rel="nofollow"> Course Description</a>)</li>

    <li style="color:green"><strong>[Pluralsight] Understanding the Java Virtual Machine: Class Loading and Reflection</strong> [ENG, 1h 58m, 28 Mar 2015] (<a href="http://www.pluralsight.com/courses/understanding-java-vm-class-loading-reflection" rel="nofollow"> Course Description</a>)</li>

    <li style="color:green"><strong>[Pluralsight] Context and Dependency Injection (CDI 1.1)</strong> [ENG, 3h 42m, 25 Mar 2015] (<a href="http://www.pluralsight.com/courses/context-dependency-injection-1-1" rel="nofollow"> Course Description</a>)</li>

    <li style="color:green"><strong>[Pluralsight] Scala for Java Developers: Syntax Comparison</strong> [ENG, 3h 18m, 26 Feb 2015]</li>
    <li style="color:green"><strong>[Pluralsight] Getting Started with Spring Data JPA</strong> [ENG, 3h 16m, 06 Feb 2015]</li>
    <li style="color:green"><strong>[Pluralsight] Introduction to the Java API for WebSockets</strong> [ENG, 2h 2m, 09 Jan 2015]</li>
</ul>


<br/><br/>
<strong>2014</strong>

<ul>
    <li style="color:green"><strong>[Pluralsight] Introduction to Spring MVC 4</strong> [ENG, 3h 46m, 28 Oct 2014]</li>
    <li style="color:green"><strong>[Pluralsight] Apache Ant Fundamentals</strong></li>
    <li style="color:green"><strong>[Pluralsight] What's New in Java 8</strong></li>
    <li style="color:green"><strong>[Pluralsight] Functional Programming With Java (<a href="https://www.pluralsight.com/courses/java-functional-programming" rel="nofollow"> Course Description</a>)</strong></li>
    <li style="color:green"><strong>[Pluralsight] Reactive Programming in Java 8 With RxJava</strong></li>
    <li style="color:green"><strong>[Pluralsight] Building Asynchronous RESTful Services With Jersey</strong></li>
    <li style="color:green"><strong>[Pluralsight] Java Web Fundamentals</strong></li>
    <li style="color:green"><strong>[Pluralsight] Java Persistence API (JPA) 2.1</strong></li>
    <li style="color:green"><strong>[Pluralsight] Cryptography Fundamentals for Java and .NET Developers</strong></li>
    <li style="color:green"><strong>[Pluralsight] Building JAX-WS Web Services with Apache CXF</strong></li>
    <li style="color:green"><strong>[Pluralsight] Test-Driven Development Practices in Java</strong></li>
    <li style="color:green"><strong>[Pluralsight] Spring Security Fundamentals</strong></li>
    <li style="color:green"><strong>[Pluralsight] Bean Validation 1.1</strong></li>
    <li style="color:green"><strong>[Pluralsight] Java Swing Development Using NetBeans</strong></li>
</ul>

<br/><br/>
<strong>2013</strong>

<ul>
    <li style="color:green"><strong>[Pluralsight] The Eclipse Guided Tour - Part 2</strong></li>
    <li style="color:green"><strong>[Pluralsight] RESTFul Services in Java using Jersey</strong></li>
    <li style="color:green"><strong>[Pluralsight] Play! 2 for Java</strong></li>
    <li style="color:green"><strong>[Pluralsight] Aspect Oriented Programming (AOP) using Spring AOP and AspectJ</strong></li>
    <li style="color:green"><strong>[Pluralsight] Spring Fundamentals</strong></li>
    <li style="color:green"><strong>[Pluralsight] The Eclipse Guided Tour - Part 1</strong></li>
    <li style="color:green"><strong>[Pluralsight] Introduction To Hibernate</strong></li>
    <li style="color:green"><strong>[Pluralsight] Making Java And C# Work Together: JVM and .NET CLR Interop</strong></li>
    <li style="color:green"><strong>[Pluralsight] Unit Testing In Java With JUnit</strong></li>
    <li style="color:green"><strong>[Pluralsight] Spring with JPA and Hibernate</strong></li>
    <li style="color:green"><strong>[Pluralsight] Getting Started With Jenkins Continuous Integration</strong></li>
    <li style="color:green"><strong>[Pluralsight] Introduction to Spring MVC</strong></li>
</ul>

<br/><br/>
<strong>2012</strong>

<ul>
    <li style="color:green"><strong>[Pluralsight] Maven Fundamentals</strong></li>
    <li style="color:green"><strong>[Pluralsight] Introduction to Struts 2</strong></li>
    <li style="color:green"><strong>[Pluralsight] Mastering Java Swing - Part 1 - 4</strong></li>
</ul>

<br/><br/>
<strong>2011</strong>

<ul>
    <li style="color:green"><strong>[Pluralsight] Java Fundamentals, Part 2</strong></li>
    <li style="color:green"><strong>[Pluralsight] Java Fundamentals, Part 1</strong></li>
</ul>


<br/><br/>
<hr/>
<br/><br/>


<strong>Android</strong>
<br/><br/>

<ul>
    <li style="color:green"><strong>[Pluralsight] Android Animationsg</strong> [ENG, 2h 13m,23 May 2015] (<a href="http://www.pluralsight.com/courses/android-animations"> Course Description</a>)</li>

    <li style="color:green"><strong>[Pluralsight] Android Photo and Video Programming</strong></li>

    <li style="color:green"><strong>[Pluralsight] Building Your First Game For Android And The PC Using Java</strong></li>

    <li style="color:green"><strong>[Pluralsight] Android Custom Components</strong></li>
</ul>
