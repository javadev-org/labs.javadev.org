---
layout: page
title: Pearson IT Certification
permalink: /library/pearson-it-Certification/java/
---

# Pearson IT Certification

### 2016

<ul>
    <li style="color:green">
        <strong>[Pearson IT Certification] Java SE 8 Programmer I Exam (1Z0-808) Complete Video Course (LiveLessons) [ENG, 19:04:46, 2016] </strong> <a href="http://www.pearsonitcertification.com/store/oca-java-se-8-programmer-i-1z0-808-complete-video-course-9780789756589" rel="nofollow">Course Description</a>
    </li>
</ul>
