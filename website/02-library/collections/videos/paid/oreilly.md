---
layout: page
title: O'Reilly Java Videos
permalink: /library/oreilly/java/
---

# O'Reilly Java Videos

<br/>

### 2016

<ul>

    <li style="color:green">
    	<strong>[O'Reilly],[infiniteskills] Spring Framework Essentials Training Video</strong> [ENG,  5 hours 14 minutes, 2016] (<a href="http://shop.oreilly.com/product/0636920046837.do" rel="nofollow">Course Description</a>, <a href="http://infiniteskills.com/training/spring-framework-essentials.html" rel="nofollow">Course Description</a>)
    </li>

</ul>

<br/>

### 2015

<ul>

    <li style="color:green">
    	<strong>[O'Reilly],[infiniteskills] Spring MVC for Java Developers Training Video</strong> [ENG,  6 hours 12 minutes, 2015]
    </li>

    <li style="color:green">
    	<strong>[O'Reilly],[infiniteskills] Building Web Services with Java
    	Working with RESTful and SOAP-based Services</strong> [ENG, 6 hours 25 minutes, 2015] (<a href="http://shop.oreilly.com/product/0636920042556.do" rel="nofollow">Course Description</a>, <a href="http://www.infiniteskills.com/training/building-java-web-services.html" rel="nofollow">Course Description</a>)
    </li>

</ul>

<br/>

### 2014

<ul>

    <li style="color:green"><strong>[O'Reilly] Enterprise Messaging with JMS: Advanced Topics and Spring JMS</strong> [ENG, 3 hrs 52 min, 2014] (<a href="http://shop.oreilly.com/product/0636920034865.do" rel="nofollow">Course Description</a> | <a href="https://bitbucket.org/javadev-org/enterprise-messaging-with-jms-advanced-topics-and-spring-jms/src/master/">src</a>)</li>

    <li style="color:green"><strong>[O'Reilly]  Enterprise Messaging JMS 1.1 and JMS 2.0 Fundamentals </strong> [ENG, 5 hours 29 minutes, 2014] (<a href="http://shop.oreilly.com/product/0636920034698.do" rel="nofollow">Course Description</a> | <a href="https://bitbucket.org/javadev-org/enterprise-messaging-jms-1.1-and-jms-2.0-fundamentals/src/master/">src</a>)</li>

</ul>
