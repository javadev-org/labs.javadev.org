---
layout: page
title: Marakana Java Videos
permalink: /library/marakana/java/java-fundamentals-and-advanced/
---


<strong>Course Summary:</strong><br/>
<!-- https://marakana.com/static/courseware/java_fundamentals/index.html<br/> -->
https://thenewcircle.com/bookshelf/java_fundamentals_tutorial/index.html<br/>

<strong>Source codes:</strong><br/>
https://github.com/marakana/java-fundamentals-20121022

<br/>

<h3>[Marakana TV] - Advanced Java Training Course [Oct 22-01 2013, ENG]</h3>

<strong>Course Summary</strong><br/>
http://marakana.com/training/java/advanced_java.html


<strong>Source codes:</strong><br/>
https://github.com/marakana/advanced-java-20130122


<pre>
Advanced Java:
Advanced Java™ Training course provides you with advanced skills for programming in Java language.
This course delves deeper into the data structures, file input and output features, exception handing, object oriented design and analysis, object serialization, database connectivity with JDBC, multi-threaded programming, and networking capabilities of Java.
Upon completion, you will be able to understand, design, and develop complex Java-based applications.
Objectives
Upon successful completion of Advanced Java™ Training course, you will be able to:

    Use reflection to examine objects at runtime and properly compare objects for equality.
    Handle errors in your program by writing exception handlers.
    Create and manipulate arrays and collections.
    Read and write files using the java.io package.
    Serialize objects using object serialization.
    Write multi-threaded programs and synchronize threads.
    Access data from relational databases using JDBC. Perform basic network communication using sockets.

Audience
You should take Advanced Java™ Training course if you are interested in learning more about the Java programming language in order to learn advanced programming techniques and concepts.
Prerequisites
To succeed fully in Advanced Java™ Training, you should be familiar and comfortable with basic Java programming concepts: Java syntax and object-oriented programming (OOP) in Java. These topics are covered in our Fundamentals of Java™ Training course.



    Examination Concepts
        Determining an Object's Type
        Using the instanceof Operator
        Using the Class.getName() Method
        Reflection
        Comparing Objects for Equality
        Converting Objects to strings and integers (hash codes)
        Sorting Objects
    Exception Handling
        Overview of Exceptions
        Following the Control Flow of an Exception Handler
        Writing Exception Handlers
        Checked vs. unchecked exceptions vs. errors
        Defining custom exceptions
        Nesting exceptions
    Data Structures
        Creating and manipulating Arrays
        Collections
        Choosing a Collection
        Working with Collections
        Manipulating Lists, Sets, Maps
        Understanding Java 5 Generics in Collections
    I/O
        Overview of the java.io Package
        Working with the File Class
        Identifying Input and Output Streams
        Identifying Readers and Writers
        Reading Data from a File
        Writing Data to a File
        Reading and Writing Objects
        Serializing an Object
    Multi-Threaded Programming
        Overview of Threads
        Observing a Computer's Processes
        Observing a Multi-Threaded Program
        Using Threads
        Deciding Between Daemon and Non-Daemon Threads
        Creating a Multi-Threaded Program
        Thread Synchronization
        The Producer/Consumer Problem
    JDBC
        Overview of SQL Language
        Overview of JDBC and its drivers
        JDBC API: connections, statements, result sets, metadata
        Using JDBC: updates, queries
    Basic Networking
        Overview of Networking
        Networking Concepts
        Identifying Your Computer's IP Address
        Using the InetAddress Class
        Sockets
        Implementing the Client Side of a Socket
        Implementing the Server Side of a Socket
        Developing a multi-threaded Server
    Design Patterns
        What are Design Patterns?
        Singleton, Factory Method, Abstract Factory
        Adapter, Composite, Decorator
        Chain of Responsibility, Observer / Publish-Subscribe, Strategy, Template
        Data Access Object (DAO)


</pre>

<br/>
<br/>

<a href="magnet:?xt=urn:btih:afde301438e44a8979bacf7293c61d78711becfa&dn=marakana.com%20-%20Java%20Fundamentals%20and%20Advanced%20%C2%AE%20vampiri6ka&tr=http%3A%2F%2Fbt3.rutracker.org%2Fann%3Fuk%3D6WROVr5o1y&tr=http%3A%2F%2Fretracker.local%2Fannounce">Java Fundamentals and Advanced Training Course (magnetlink)</a>



