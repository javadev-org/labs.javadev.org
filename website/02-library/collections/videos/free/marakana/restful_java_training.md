---
layout: page
title: Marakana Java Videos
permalink: /library/marakana/java/restful-java-training/
---


<strong>[Marakana TV] - RESTful Java Training Course (Jersey) [ENG]</strong><br/><br/>


<strong>Videos and source codes:</strong><br/>
https://github.com/marley-java/marakana.com-RESTful-Java

<br/>
<br/>

Release-date: January 2012<br/>
Run-time: 3 day course, every day has 4 videos that are about 1 hour long.<br/>
Video format: .mov (QuickTime Movie)<br/>
Resolution: 800x600 and 1024x768<br/>
<br/><br/>

<p>
The three day Java Web Services course provides a thorough guide to the design and implementation of RESTful web services using Java. We take students from the fundamental concepts of the REST architectural style and its embodiment in existing web standards, all the way through the creation and testing of a REST service. The course also illustrates how the decades-old HTTP protocol can in fact elegantly address a variety of "enterprise scale" issues, including high concurrency and atomic transactions.
</p>

<pre>

Outline
1. Concepts: REST vs. the Rest
  The Richardson Maturity Model
  Resources: identification, representations and manipulation
  Hypermedia as engine of application state
2. HTTP Basics
   "CRUD" mapped to verbs / methods
    Introduction to HTTP Headers
    A glance at Webmachine's "Big HTTP Graph"
3. Intro to JAX-RS and Jersey
   Creating resource classes
   Jersey annotations: HTTP methods and headers
   Constructing responses: headers and bodies
   Standalone deployment with SimpleServerFactory
   Testing fundamentals: JUnit and Jersey-Client
4. Resource Representations
   Standard internet media types
   JAXB: standard annotations and "content negotiation" for XML and JSON
   Jackson: a non-standard, JSON-optimized alternative
   More testing: Jackson object mapping and Hamcrest pattern matching
5. Representation Recipes
   Building links to related resources, and URI templates for families of resources
   Aggregate representations
   Combating representation bloat: summaries, expansions and pagination
6. "Enterprise" HTTP
   Authentication
   Optimizations: conditional requests (using modification dates and entity tags) and    
   caching
   Transactions: ephemeral resources
   When does a GET have to be a POST?

</pre>


<br/>
<br/>

<a href="magnet:?xt=urn:btih:20bde47efc4616a451a83b55e3d782b092e50349&dn=marakana.com%20-%20RESTful%20Java%20%C2%AE%20vampiri6ka&tr=http%3A%2F%2Fbt4.rutracker.org%2Fann%3Fuk%3DGb8OjPC4Gc&tr=http%3A%2F%2Fretracker.local%2Fannounce"> RESTful Java Training Course (magnetlink)</a>



