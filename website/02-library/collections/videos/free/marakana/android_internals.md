---
layout: page
title: Marakana Java Videos
permalink: /library/marakana/java/android-internals/
---


# [Marakana TV] - Android Internals [Oct 22-01 2013, ENG]

<!--
<strong>Course Summary</strong><br/>
http://marakana.com/training/java/advanced_java.html

-->


Android Internals training course is designed for those who are already familiar with basics of Android SDK and are looking to customize and/or extend the functionality of the platform.


Android Internals focuses on Android NDK and Android IDL APIs to give you a clean access to the underlying hardware and services with future compatibility in mind. You will learn how to build custom images and hack the platform.

<br/>
<strong>Objectives</strong>


By completing Android Internals training course you will be able to:


<ul>
    <li>Explain the anatomy of the Android platform and get is physiology (layer interactions)</li>
    <li>Build native applications in Android using JNI and NDK</li>
    <li>Take advantage of Android AIDL to build IPC-enabled bound services</li>
    <li>Build the entire Android platform from source and get what's what</li>
    <li>Customize and extend the Android platform to build custom ROMs</li>
    <li>Modify and extend Android frameworks and services</li>
    <li>Take advantage of custom hardware with Android</li>
    <li>Understand where Android departs from standard Linux</li>
</ul>


<br/>
<strong>Audience</strong>


This Android Internals course is for developers who want to dig deeper than the standard Android SDK. It is for those who want to hack the system a bit in order to add system services and hardware support for non-standard components or port Android to completely new boards.

<br/>
<strong>Prerequisites</strong>


<pre>

<strong>Java</strong>
To take this course, you must know Java. You should be able to answer most of the following questions:

    What is a difference between a class and an object?
    What is the difference between static and non-static field?
    What is the difference between extends and implements keywords?
    What is an anonymous inner class?
    What is the purpose of @Override?

To refresh your Java skills you can review <a href="https://thenewcircle.com/static/bookshelf/java_fundamentals_tutorial/index.html">Marakana Java Tutorial</a> to get up to speed.
Additionally, knowledge of Eclipse is required. You could watch <a href="https://thenewcircle.com/s/post/595/video_introduction_to_eclipse_driving_java_productivity">this 30-minute tutorial</a> to get up to speed with Eclipse.

<strong>C/C++/Make</strong>

To get the most benefit from this class you must have a basic understanding of C and C++. For example, you should be able to answer the following:

    What is a header file?
    What is gcc and how to use it?
    Basic usage of sprintf()
    What is make and how does it work?
    Be able to read and understand basic Makefiles.
    Be able to read and understand shell scripts.

<strong>Linux</strong>
You should be familiar with basic Linux operating system. For example, you should be able to answer the most of the following:

    How do you use use the following commands: ls, ps, cp, mv, pwd, cat, chmod, chown, mount, and similar.
    What is the init process?
    What are users and groups in Linux and how do r/w/x permissions work?


</pre>


<br/>
<strong>Cource Summary</strong>

<pre>

    Android Overview
    Android Stack
        Android Kernel Layer
        Android Native Layer
        Android Application Framework Layer
        Android Applications Layer
    Java Native Interface (JNI) and the Android Native Development Kit (NDK)
    Android Inter-Process Communication (IPC) with Binder and AIDL
    Android Security Essentials
    Building Android from Source
    Android Startup
    Android Subsystems
    Creating a Customized Android System Image
    Android Tools and Debugging

</pre>

<br/>

<strong>Videos and source codes:</strong><br/>
https://github.com/marakana/internals-2012-10-09


<br/>

<a href="magnet:?xt=urn:btih:4df88cee0f48ad2cd0b53a244a675ba4bfb633bb&dn=marakana.com%20-%20Android%20Internals%20%C2%AE%20vampiri6ka&tr=http%3A%2F%2Fbt3.rutracker.org%2Fann%3Fuk%3D6WROVr5o1y&tr=http%3A%2F%2Fretracker.local%2Fannounce"> Android Internals (magnetlink)</a>
