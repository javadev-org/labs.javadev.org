---
layout: page
title: Marakana Java Videos
permalink: /library/marakana/java/android-bootcamp-training/
---


<strong>[Marakana TV] - Android Bootcamp Training [16.04.2012, ENG]</strong><br/><br/>

<p>

Android™ Bootcamp Training Course is a hands-on training for designing and building mobile applications using Android™ open-source platform. Android™ Bootcamp course explains the philosophy of developing for Android™ through its main application development building blocks and their interaction with one another.

<br/><br/>

This complete hands-on course encourages students to learn by building increasingly more sophisticated and meaningful mobile applications for Android™.

<br/><br/>

By the end of the course, each participant will build their own complete Android application incorporating most of the key aspects of the platform. Typically, we build a Twitter app for Android, but there are other choices depending on participants' interests.

<br/><br/>

This course has been updated for Ice Cream Sandwich.

<br/><br/>
<strong>Sources:</strong><br/>
https://github.com/marakana/bootcamp-2012-04-16

</p>

<br/><br/>
<hr>
<br/><br/>

<br/>
<br/>

<a href="magnet:?xt=urn:btih:1dfaf9c975bd7cf6aa1370ea6a8f944d76de28a8&dn=Marakana%20Bootcamp-2012-04-16&tr=http%3A%2F%2Fbt2.rutracker.org%2Fann%3Fuk%3D6WROVr5o1y&tr=http%3A%2F%2Fretracker.local%2Fannounce">Android Bootcamp Training (magnetlink)</a>





