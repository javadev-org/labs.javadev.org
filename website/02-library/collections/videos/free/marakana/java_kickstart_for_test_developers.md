---
layout: page
title: Marakana Java Videos
permalink: /library/marakana/java/java-kickstart-for-test-developers/
---


<strong>[marakana.com] Java Kickstart for Test Developers [2012, ENG]</strong><br/><br/>

<xmp>
    Java basics (1.5 days)
        Development environment: JDK, Eclipse
        Primitives vs. objects, values vs. references
        Variables, methods and control flow
        Classes: static vs. instance fields / methods
        Bean naming conventions: getters, setters - Interfaces and polymorphism
        Abstract classes and inheritance
        Enumerations
    Collections, design and best practices (1 day)
        Packages, imports, static imports
        Generics, and standard collections: List, Set, Map
        Navigating standard Javadoc
        Iterators and the Java 5 "foreach" loop
        "Value classes" vs. "Service classes"
        Immutability: final fields - Object.equals(), hashCode(), toString()
    IO and threading basics (1/2 day)
        IO: Files, input and output streams, readers and writers
        Proper exception handling and resource management
        Threads: start, join, executors, futures
        Discussion about issues in concurrent programming

</xmp>

<br/>
<br/>

<a href="magnet:?xt=urn:btih:f43ccc66967322fccf202549e4cd4b4d3be7031e&dn=marakana.com%20-%20Java%20Kickstart%20for%20Test%20Developers%20%C2%AE%20vampiri6ka&tr=http%3A%2F%2Fbt3.rutracker.org%2Fann%3Fuk%3D6WROVr5o1y&tr=http%3A%2F%2Fretracker.local%2Fannounce"> Java Kickstart for Test Developers (magnetlink)</a>

