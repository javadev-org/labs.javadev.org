---
layout: page
title: TheNewBoston
permalink: /library/thenewboston/java/free/
---


<h2>[thenewboston] JavaFX Java GUI Design Tutorials</h2><br/>


<div align="center">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL6gx4Cwl9DGBzfXLWLSYVy8EbTdpGbUIG" frameborder="0" allowfullscreen></iframe>
</div>

<br/><br/>
<hr/>
<br/><br/>

<h2>[thenewboston] Java Programming Tutorial</h2><br/>


<div align="center">
    <iframe width="640" height="360" src="//www.youtube.com/embed/Hl-zzrqQoSE?list=PL0DA31D88FC21E462" frameborder="0" allowfullscreen></iframe>
</div>
