---
layout: page
title: Java Brains
permalink: /library/java_brains/free/
---

<div>


<h2>[Java Brains] - REST Web Services [2014, ENG]</h2><br/>

<br/>
<br/>

<div align="center">
    <iframe width="560" height="315" src="//www.youtube.com/embed/xkKcdK1u95s" frameborder="0" allowfullscreen></iframe>
</div>



<br/>
<br/>

</div>


<br/>
<br/>
<hr/>
<br/>
<br/>



<div>


<h2>[Java Brains] - SOAP Web Services [2013, ENG]</h2><br/>

<br/>
<br/>


<div align="center">
    <iframe width="560" height="315" src="http://www.youtube.com/embed/mKjvKPlb1rA" frameborder="0" allowfullscreen></iframe>
</div>
