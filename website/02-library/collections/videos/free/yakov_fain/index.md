---
layout: page
title: Yakov Fain
permalink: /library/yakov_fain/java/free/
---

<div align="center">
    <iframe width="480" height="360" src="//www.youtube.com/embed/llCoszN5kcc" frameborder="0" allowfullscreen></iframe>
</div>


<br/>
<br/>
http://www.youtube.com/user/yfain/videos
<br/>
<br/>

<pre>
Course Outline

Unit 1. Introducing Java
Why learning Java?
The life cycle of a Java program
JDK and JRE
Downloading and installing JDK
Your first Java program: Hello World
Java SE and EE editions
Eclipse IDE and your first Java program
Introducing Eclipse Integrated Development Environment
Different versions of Eclipse IDE

Unit 2. Object-Oriented Programming. Version Control with Git.
Classes and objects
Data types: primitives and wrapper classes
Autoboxing
Variable scopes
Java Comments
Method arguments
Method overloading
Intro to Git and GitHub

Unit 3. Class methods
Constructors
The keyword super
The keyword this
Pass by value or by reference
Variable Scopes
The keyword static
Arrays
Command-Line Arguments
If statements
Switch statement
Debugging Java programs
Packages
Working with Git and GitHub

Unit 4. Interfaces, Abstract classes, Polymorphism
Interfaces
Casting
Marker interfaces
Access levels
The keyword final
Abstract classes
Polymorphism
Raising salaries with abstract classes
Raising salaries with interfaces

Unit 5. Introducing the Graphic User Interface
Basic UI components
Layout Managers
Adapters
Inner Classes
Containers
Developing UI for a calculator
Event listeners
Teaching the calculator to calculate
Inner Classes

Unit 6. Web applications with Applets
What’s the difference between Java Applications and Applets
Quick intro to HTML
Embedding an applet in HTML
Applet life cycle
Inner Classes
Introducing Swing
Developing a Tic-Tac-Toe applet
Deploying the Tic-Tac-Toe on the server

Unit 7. Error processing. Data Structures
Reading the Stack Trace
The hierarchy of exceptions
The keywords try, catch, throws, throw, finally
User-Defined Exceptions
Array
ArrayList
Hashtable and HashMap
Enumeration
Properties
Linked Lists

Unit 8. Generics. Streams
Introduction to generics
Defining generics
Generic methods
Working with files and other streams
Byte Streams
Character Streams
Data streams
Stream tokenizers
Class File

Unit 9. Java Serialization
How JVM’s talk to each other
Object Streams
What’s Java Serialization
Interface Serializable
Interface Externalizable
Network Programming
Reading data from the Internet
Connecting through HTTP Proxy Servers
How to download files from the Internet

Unit 10. Sockets. Introduction to Multithreading
Developing a sample Stock Quote program
Socket Programming
The Stock Quote server with sockets
How to run the Stock Quote server
Class Thread
Interface Runnable
Thread States
Sleeping threads
Thread priorities

Unit 11. More about threads
How to stop a thread
Race conditions and thread synchronization
Using Threads with Swing: SwingWorker
Joining threads
Futures
Goodies from java.util.concurrent

Unit 12. Working With Databases Using JDBC
JDBC Driver Types
Sample JDBC Program
Processing Result Sets
Class ResultSetMetaData
Class CallableStatement
Batch Updates
Data sources and connection pools
My brokerage firm with DBMS

Unit 13. More Swing. Annotations. Reflection
Displaying tabular data with JTable
Using Table Models
Persisting data from JTable
Working with TableCellRenderer Interface
Event Dispatch thread and SwingWorker class
Intro to annotations
Compiler annotation
Annotation Processing

Unit 14. Remote Method Invocation. Java EE 6. Servlets
Defining Remote Interfaces
Implementing Remote Interfaces
Registering Remote Objects
Writing RMI Clients
Finding Remote Objects
Setting Up The Stock Server Application
Java EE 6 overview
Java EE component overview
Containers vs application servers
Installing GlassFish 3 server
Java Servlets
Architecture of Web applications with Java servlets
The browser-servlet data flow
Your first servlet

Unit 15. Sessions. Cookies. Java Server Pages
Deploying servlets as WAR files
HTTP Get and Post requests
Session Tracking With servlets
Cookies
Session Tracking API – HttpSession
Advantages of JSP over servlets
Embedding Java Code Into HTML
Major JSP Tags
Implicit JSP Objects
Error Pages
Stock Portfolio Project With JSP
Deploying JSP

Unit 16. Java Messaging Service. Enterprise Java Beans
Advantages of asynchronous communication
Point-to-point and Publish/Subscribe modes
Message-Oriented Middleware (MOM)
Types of Messages
How to Send a Message
How to Receive a Message
How to Publish a Message
How to Subscribe for a Topic
Message Selectors
Intro to EJB 3.1
Role of an EJB Container
Session beans
Message-Driven Beans

Unit 17.Java Persistense API 2.0. Restful Web Services
Entity classes
Primary keys
The EntityManager
Querying entities with JPQL
What are Web Services
SOAP vs REST
Sample application with REST

Unit 18. Java 8. New Features
Lambda Expressions
Collections and Stream API
Functional Interfaces
</pre>


<br/>
<br/>
slides:  
https://code.google.com/p/practicaljava/wiki/Slides


<br/>
<br/>
<hr/>
<br/>
<br/>


<a href="magnet:?xt=urn:btih:fcfc5d84f813108f10533c3c535c58ccf844463d&dn=Yakov%20Fain%20-%20Java%20Training%20Videos&tr=http%3A%2F%2Fbt.rutracker.org%2Fann%3Fuk%3D6WROVr5o1y&tr=http%3A%2F%2Fretracker.local%2Fannounce">Magnetlink</a>

<hr/>
<br/>
<br/>
