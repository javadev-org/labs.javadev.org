---
layout: page
title: Java Magazine
permalink: /library/magazines/java_magazine/
---

# Java Magazine

<br/>
<strong>2017</strong>
<br/>

<ul>
    <li><a href="http://www.javamagazine.mozaicreader.com/MarApr2017">March/April</a></li>
    <li><a href="http://www.javamagazine.mozaicreader.com/JanFeb2017">January/February</a></li>
</ul>

<br/>
<strong>2016</strong>
<br/>

<ul>
    <li><a href="/files/pdf/magazines/JavaMagazine/2016/JavaMagazine_NovemberDecember_2016.pdf">November/December</a></li>
    <li><a href="/files/pdf/magazines/JavaMagazine/2016/JavaMagazine_SeptemberOctober_2016.pdf">September/October</a></li>
    <li><a href="/files/pdf/magazines/JavaMagazine/2016/JavaMagazine_JulyAugust_2016.pdf">July/August</a></li>
    <li><a href="/files/pdf/magazines/JavaMagazine/2016/JavaMagazine_MayJune_2016.pdf">May/June</a></li>
    <li><a href="/files/pdf/magazines/JavaMagazine/2016/JavaMagazine_MarchApril_2016.pdf">March/April</a></li>
    <li><a href="http://click.digital.halldata.com/?qs=9905ffdae32461ad7c2b4a2c03eb78f109203973e203466da5094c71f717400ab139c88f81e2dd87">January/February</a></li>
</ul>

<br/>

I think i have all magazines.

<!--

<br/>
<br/>
<strong>2015</strong>
<br/>
<br/>

<ul>

    <li><a href="http://www.javamagazine.mozaicreader.com/JanFeb2016">November/December</a></li>

    <li><a href="http://www.oraclejavamagazine-digital.com/javamagazine/september_october_2015">September/October</a></li>

    <li><a href="http://mailings1.gtxcel.com/portal/wts/ucmc%7CS%7CfmAkqegkjBqjvsB3qnb0Fv-dsOl92H0KkHn58g">July/August</a></li>

    <li><a href="http://mailings1.gtxcel.com/portal/wts/ucmc%7CS%7CfjEAqeg%7C0D6jt68%5E6m-vT3%7CdsOl92H0KkHn58g">May/June</a></li>

    <li><a href="http://mailings1.gtxcel.com/portal/wts/ucmc%7CS%7CfgBBaegeV3ajrzj66kA8-7AdsOl92H0KkHn58g">March/April</a></li>
    <li><a href="http://mailings1.gtxcel.com/portal/wts/ucmc%7CS%7Cfe8qLefDSz6j%7CBCs6kj7%7CfOdsOl92H0KkHn58g">January/February</a></li>
</ul>

<br/>
<br/>
<strong>2014</strong>
<br/>
<br/>

<ul>
<li><a href="http://mailings1.gtxcel.com/portal/wts/ucmc%7CS%7CeDC8qef-7-qjf6F7ajOnRo9dsOl92H0KkHn58g">November/December</a></li>
<li><a href="http://mailings1.gtxcel.com/portal/wts/ucmc%7CS%7Ce-O-6ef4D26jcEMeqj030qPdsOl92H0KkHn58g">September/October</a></li>
<li><a href="http://mailings1.gtxcel.com/portal/wts/ucmc%7CS%7Ce4BfLefzsza%7CT-zfLjcB7qsdsOl92H0KkHn58g">July/August</a></li>
<li><a href="http://mailings1.gtxcel.com/portal/wts/ucmc%7CS%7Ce282Lefve%5Eq%7CRkbFa%7CFmv%7C%3BdsOl92H0KkHn58g">May/June</a></li>
<li><a href="http://mailings1.gtxcel.com/portal/wts/ccmciSiewg26efodeLiNsz86irAsv8dsOl92H0KkHn58g">March/April</a></li>
<li><a href="http://mailings1.gtxcel.com/portal/wts/cgmciSierPzLefjd7LiD0r8qAzDCraPHNTSlgGNGdxTBa">January/February</a></li>
</ul>

<br/>
<br/>
<strong>2013</strong>
<br/>
<br/>

<ul>
<li><a href="http://mailings1.gtxcel.com/portal/wts/cgmciSienDzLefeskaiAfFBq6TFtr6PHNTSlgGNGdxTBa">November/December</a></li>
<li><a href="http://mailings1.gtxcel.com/portal/wts/cgmciSiej9vqeeBc46i7rgs62todEaPHNTSlgGNGdxTBa">September/October</a></li>
<li><a href="http://mailings1.gtxcel.com/portal/wts/cgmciSiefM-Lee7DFaiygzvqyxnNiLPHNTSlgGNGdxTBa">July/August</a></li>
<li><a href="http://mailings1.gtxcel.com/portal/wts/cgmciSidFgAaee3-eqiviRBqu0rrg6PHNTSlgGNGdxTBa">May/June</a></li>
<li><a href="http://mailings1.texterity.com/portal/wts/cemciSidAgrLeewu0qFAsB6dzT2C6nlmJnNMFXFLQZjc">March/April</a></li>
<li><a href="http://mailings1.texterity.com/portal/wts/cemciSid7h0aeerFkqECrwyc3Q7u6nlmJnNMFXFLQZjc">January/February</a></li>
</ul>

<br/>
<br/>
<strong>2012</strong>
<br/>
<br/>

<ul>
<li><a href="http://mailings1.texterity.com/portal/wts/ccmciSid2C-LeemveLDrs8ii3NDMdsOl92H0KkHn58g">November/December</a></li>
<li><a href="http://mailings1.texterity.com/portal/wts/ccmciSidzknLeeitm6AOEdudF0m9dsOl92H0KkHn58g">September/October</a></li>
<li><a href="http://www.oraclejavamagazine-digital.com/t=c/?1&197194&97914&38261&0000&16786297&ICiSb9m9aNY5">July/August</a></li>
<li><a href="http://www.oraclejavamagazine-digital.com/t=c/?1&192378&93443&37080&0000&16783964&ICiSb9m9aNY5">May/June</a></li>
<li><a href="http://www.oraclejavamagazine-digital.com/t=c/?1&187183&77671&35694&0000&16781418&ICiSb9m9aNY5">March/April</a></li>
<li><a href="http://www.oraclejavamagazine-digital.com/t=c/?1&183410&68032&34657&0000&16779555&ICiSb9m9aNY5">January/February</a></li>
</ul>


<br/>
<br/>
<strong>2011</strong>
<br/>
<br/>

<ul>
<li><a href="http://www.oraclejavamagazine-digital.com/t=c/?1&175852&54882&32686&0000&1236516792&ICiSb9m9aNY5">November/December</a></li>
<li><a href="http://www.oraclejavamagazine-digital.com/t=c/?1&165297&22778&30072&0000&1236511694&ICiSb9m9aNY5">Premiere</a></li>

</ul>

-->
