---
layout: page
title: Java Conferences
permalink: /conferences/
---



<h1>JavaOne - Development Tools and Techniques (2012)</h1>
All videos H.264, 1,280 x 720

<ul>
    <li>101 Ways to Improve Java - Why Developer Participation Matters</li>
    <li>A Common Development Framework - From JSP to GSP at CERN</li>
    <li>A Deep Dive into Java Performance Analysis with Advanced Toolsets</li>
    <li>Autosharding Enterprise to Social Gaming Applications with NoSQL and Couchbase</li>
    <li>Behavior-Driven Development on the JVM - A State of the Union</li>
    <li>Bringing Mylyn to NetBeans and OSGi Bridging Their Worlds</li>
    <li>Building Content-Rich Java Apps in the Cloud or On-Premises with the Alfresco API</li>
    <li>Building Native iPhone iPad Applications in Java CodeName One</li>
    <li>Building Portable Java EE 6 Applications in Eclipse</li>
    <li>Building Rich Client Applications with Eclipse 4</li>
    <li>Cloud ALM - Connecting the Development Team to the New Deployment Destination</li>
    <li>Code Inspection with the javac</li>
    <li>Concurrency Without Pain in Pure Java</li>
    <li>Concurrent Programming with the Disruptor</li>
    <li>Cover Your Web and Mobile Applications with Integration Tests from Scratch</li>
    <li>Cross-Build Injection Attacks - How Safe Is Your Java Build</li>
    <li>Custom Declarative Refactoring</li>
    <li>Custom Static Code Analysis</li>
    <li>Delivering Bug-Free More Efficient Code for the Java Platform</li>
    <li>Detecting Memory Leaks in Applications Spanning Multiple JVMs</li>
    <li>Developing Best Practices for Using Hudson CI</li>
    <li>Doing JSF Development in the NetBeans IDE</li>
    <li>Dynamic Class Reloading in the Wild with Javeleon</li>
    <li>Dynamic Instrumentation of Java Statically Defined Tracing</li>
    <li>Eclipse 4.x - A Major Upgrade of Eclipse</li>
    <li>Efficient Java Code and Project Organization</li>
    <li>First Do No Harm - Deferred Checked Exception Handling Promotes Reliability</li>
    <li>Gaining Market Advantage via Simplification and Differentiation with Java</li>
    <li>How My Life Would Have Been So Much Better If We Had Used the NetBeans Platform</li>
    <li>IDE 2.0 - How Much Can the IDE Predict You Will Write in the Next Few Seconds</li>
    <li>Improve Your Java Code Functional-Style Now</li>
    <li>Innovative Testing Techniques with Bytecode Instrumentation</li>
    <li>Introduction to the Play Framework</li>
    <li>JEDI - The Catalyst for Java Education</li>
    <li>Java Application Design Practices to Avoid When Dealing with Sub-100-Millisecond SLAs</li>
    <li>Java EE and Spring Framework Panel Discussion</li>
    <li>Kinect Open Source Programming Secrets - Hacking with OpenNI NITE and Java</li>
    <li>Large-Scale Automation with Jenkins</li>
    <li>Lessons Learned in Building Enterprise and Desktop Applications with the NetBeans IDE</li>
    <li>LinkedIn - A Social Network Built with Java Technologies and Agile Practices</li>
    <li>Linking the Java Application Lifecycle with Hudson</li>
    <li>Mixed-Language Development - Leveraging Native Code from Java</li>
    <li>Modern Software Development Antipatterns</li>
    <li>NetBeans.Next - The Roadmap Ahead</li>
    <li>NetBeans Platform Panel Discussion</li>
    <li>NetBeans Plug-in Development - JRebel Experience Report</li>
    <li>OSGi for the Earthings - Meet the Eclipse Libra</li>
    <li>Open Source Identity and Access Management Expert Panel</li>
    <li>Oracle JDeveloper and Oracle ADF - What's New</li>
    <li>Patterns for Modularity - What Modules Don t Want You to Know</li>
    <li>Pimp Your Productivity with Git Gerrit Hudson and Mylyn</li>
    <li>Project EASEL - Developing and Managing HTML5 in a Java World</li>
    <li>Protecting Machine-Only Processed Data</li>
    <li>Real-Life Load Testing - Up to Your Knees in Mud</li>
    <li>Real-World Strategies for Continuous Delivery with Maven and Jenkins</li>
    <li>Rediscovering Your Architecture Through Software Archaeology</li>
    <li>Resource Management in Java - Tips Traps and Techniques</li>
    <li>Right Ways to Climb the OpenJDK Source Tree</li>
    <li>Runtime Class Reloading for Dummies</li>
    <li>Server-Supported Large-Scale Development</li>
    <li>Software Modularity - Paradoxes Principles and Architectures</li>
    <li>Spearfish - Real-Time Java-Based Underwater Tracking of Large Numbers of Targets</li>
    <li>Strategies for Testing Event-Driven Programs</li>
    <li>Take Performance Tuning of Your Enterprise Java Applications to the Next Level</li>
    <li>Taming the Spaghetti - Rich Web Applications with Errai</li>
    <li>The Arquillian Universe - A Tour Around the Astrophysics Lab</li>
    <li>The Future of ALM - Developing in the Social Code Graph</li>
    <li>The Polyglot Java VM and Java Middleware</li>
    <li>The Power of Java 7 NIO.2</li>
    <li>The Social Developer</li>
    <li>Thermostat - An Open Source Instrumentation Tool for the HotSpot JVM</li>
    <li>Tooling Support for Enterprise Development</li>
    <li>Tools for Java EE 7 - Evolution or Revolution</li>
    <li>Uncovering Hidden Power Tools in the JDK</li>
    <li>Why There's No Future in Java Futures</li>
</ul>

<br/><br/>

<a href="magnet:?xt=urn:btih:6f84a6028c34b01a73ca629e6a1f670f9a0e4766&dn=JavaOne%20-%20Development%20Tools%20and%20Techniques%20%282012%29&tr=http%3A%2F%2Fbt2.rutracker.org%2Fann%3Fuk%3D6WROVr5o1y&tr=http%3A%2F%2Fretracker.local%2Fannounce">magnetlink</a>
