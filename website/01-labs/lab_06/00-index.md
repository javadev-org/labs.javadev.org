---
layout: page
title: Lab 6 Selenium Grid Understanding and Configuring
permalink: /labs/lab-06/
---

# [Lab 6] Selenium Grid Understanding and Configuring

<br/>


Selenium Greed allows you run your tests on different machines against different browsers in parallel. That is, running multiple tests at the same time against different machines running different browsers and operating system. Essentially, Selenium Grid support distributed test execution.

<br/>


**Selenium installation:**  
http://javadev.org/development-tools/testing/selenium/installation/centos/7/


<br/>

**My Eclipse Sources:**  
https://github.com/javalabs-org/lab-06-selenium-grid/

<br/>

**Selenium versions:**
http://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-firefox-driver


<br/>

<div align="center">

<iframe width="853" height="480" src="https://www.youtube.com/embed/g1IYmdmsCkQ?list=PL6tu16kXT9Po4YMQz_uEd5FN4V3UyAZi6" frameborder="0" allowfullscreen></iframe>

</div>

<br/>

**Start as Server (Hub) (192.168.1.11):**

    $ java -jar /opt/selenium/selenium -role hub

    Nodes should register to http://192.168.1.11:4444/grid/register/


We can connect to hub by browsers:  
http://192.168.1.11:4444/grid/console/


<br/>

**Start as Node (192.168.1.6):**  

    $ java -jar /opt/selenium/selenium -role webdriver -hub http://192.168.1.11:4444/grid/register/ -port 5557


We can connect to hub by browsers:  
http://192.168.1.6:5557/wd/hub/


<br/>



Works well, when I'm work with remote Windows 7, but when I'm work with ubuntu 14.04, I'm receiving next exception (And i do not know how to fix it):


---


    Build info: version: '2.53.0', revision: '35ae25b1534ae328c771e0856c93e187490ca824', time: '2016-03-15 10:43:46'
    System info: host: 'workstation', ip: '127.0.1.1', os.name: 'Linux', os.arch: 'amd64', os.version: '4.2.0-27-generic', java.version: '1.8.0_91'
    Driver info: org.openqa.selenium.remote.RemoteWebDriver
        at sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)
        at sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)
        at sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)
        at java.lang.reflect.Constructor.newInstance(Constructor.java:423)
        at org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:206)
        at org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:158)
        at org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:678)
        at org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:249)
        at org.openqa.selenium.remote.RemoteWebDriver.<init>(RemoteWebDriver.java:131)
        at org.openqa.selenium.remote.RemoteWebDriver.<init>(RemoteWebDriver.java:158)
        at org.javalabs.selenium.grid.StarterOfGrid.main(StarterOfGrid.java:20)
    Caused by: org.openqa.selenium.remote.ErrorHandler$UnknownServerException: Unable to connect to host 127.0.0.1 on port 7055 after 45000 ms. Firefox console output:
    u-paket för Firefox.","creator":"Canonical Ltd.","homepageURL":null},{"locales":["uk"],"name":"Ubuntu Modifications","description":"Убунтівські доповнення до Firefox.","creator":"Canonical Ltd.","homepageURL":null},{"locales":["zh-CN"],"name":"Ubuntu Modifications","description":"Ubuntu 火狐扩展包.","creator":"Canonical Ltd.","homepageURL":null},{"locales":["zh-TW"],"name":"Ubuntu Modifications","description":"Ubuntu Firefox 擴充包。","creator":"Canonical Ltd.","homepageURL":null}],"targetApplications":[{"id":"{ec8030f7-c20a-464f-9b0e-13a3a9e97384}","minVersion":"9.0","maxVersion":"37.0a1"}],"targetPlatforms":[],"multiprocessCompatible":false,"signedState":2,"seen":true}
    1465735249209	DeferredSave.extensions.json	DEBUG	Save changes



<br/>

# Parallel Execution (I don't test this)

Install TestNG pluggin for Eclipse:

help --> Install New Software

Add:

http://beust.com/eclipse

Run ParallelTests.java

Console Output:

    [TestNG] Running:
      /tmp/testng-eclipse-235466463/testng-customsuite.xml

<br/>


    $ cat /tmp/testng-eclipse-235466463/testng-customsuite.xml
    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd">
    <suite name="Default suite">
      <test verbose="2" name="Default test">
        <classes>
          <class name="org.javalabs.selenium.grid.ParallelTests"/>
        </classes>
      </test> <!-- Default test -->
    </suite> <!-- Default suite -->

<br/>


This file has been upated and moved to resource folder.


    <?xml version="1.0" encoding="UTF-8"?>
    <!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd">
    <suite name="Default suite" thread-count="4" parallels="tests">

      <test verbose="2" name="Win10">
        <parameters>
             <parameter name="platform" value="WIN8_1" />
             <parameter name="browserName" value="chrome" />
             <parameter name="remoteurl" value="http://192.168.1.6:5557/wd/hub" />
        </parameters>
        <classes>
          <class name="org.javalabs.selenium.grid.ParallelTests">
            <methods>
                <include name="googleSearch" />
                <include name="clickExecuteAutomation" />
            </methods>
          </class>
        </classes>
      </test>

      <test verbose="2" name="Win8_1">
        <parameters>
             <parameter name="platform" value="WIN8_1" />
             <parameter name="browserName" value="firefox" />
             <parameter name="remoteurl" value="http://192.168.1.7:5557/wd/hub" />
        </parameters>
        <classes>
          <class name="org.javalabs.selenium.grid.ParallelTests"/>
        </classes>
      </test>
    </suite>


<br/>

Before run, need to add to Path crome driver for selenium.
