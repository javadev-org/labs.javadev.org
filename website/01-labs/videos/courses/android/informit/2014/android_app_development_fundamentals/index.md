---
layout: page
title: Android App Development Fundamentals I and II LiveLessons
permalink: /labs/informit/2014/android_app_development_fundamentals/

---


In progress


<strong>Paul J. Deitel - Android App Development Fundamentals II, Second Edition [2014, ENG] </strong> <a href="http://www.informit.com/store/android-app-development-fundamentals-i-and-ii-livelessons-9780133904338">Course Description</a>


### Source Codes:

https://github.com/android-labs/Android-App-Development-Fundamentals  

To work with projects, you should download source codes.  
Then import projects in the Android Studio.


### Projects in the Course:

<a href="/labs/courses/android/informit/2014/android_app_development_fundamentals/doodlz/">Doodlz</a>  

<a href="/labs/courses/android/informit/2014/android_app_development_fundamentals/welcome/">Welcome</a>  

<a href="/labs/courses/android/informit/2014/android_app_development_fundamentals/tipcalculator/">TipCalculator</a>  

<a href="/labs/courses/android/informit/2014/android_app_development_fundamentals/twittersearches/">TwitterSearches</a>  
