---
layout: page
title: Developing Android Apps (Android Fundamentals)
permalink: /labs/udacity/2014/developing_android_apps/
---

Link on developer.android.com:  
http://developer.android.com/training/index.html

Link on udacity:  
https://www.udacity.com/course/developing-android-apps--ud853

Android Studio and Android SDK:  
https://developer.android.com/sdk/index.html

Original Source Codes:  
https://github.com/udacity/Sunshine

My Source Codes:  
https://github.com/android-labs/Sunshine.git

<br/>

ADB - Android Debug Bridge  
AVD - Android Virtual Device  

<br/>


### Course Lessons:


<a href="http://javadev.org/java_basics/android/installation/">Prepare Ubuntu Linux Environment for Development Android Apps with Android Studio</a>  

<a href="/labs/udacity/2014/developing_android_apps/1/">Lesson 1 - Create Project Sunshine</a>  


<a href="/labs/udacity/2014/developing_android_apps/2/">Lesson 2 - Connect Sunshine to the Cloud</a>

In progress  

<a href="/labs/udacity/2014/developing_android_apps/3/">Lesson 3 - New Activities and Intents Videos</a>
