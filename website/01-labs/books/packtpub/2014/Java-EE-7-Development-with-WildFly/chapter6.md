---
layout: page
title: Chapter 6. Developing Applications with JBoss JMS Provider
permalink: /labs/books/packtpub/2014/java-ee-7-development-with-wildfly/6/
---

### A real-world example – HornetQ and ActiveMQ integration

I setup wildfly with active mq integreation like here.  
http://javadev.org/docs/appserv/wildfly/8.2/active-mq//wildfly-activemq-integration-as-module/

<br/>
standalone-full.xml
<br/>

    <subsystem xmlns="urn:jboss:domain:resource-adapters:2.0">
        <resource-adapters>
            <resource-adapter id="activemq-5.11.1">
                <module slot="main" id="org.apache.activemq"/>
                <transaction-support>NoTransaction</transaction-support>
                <config-property name="ServerUrl">
                    tcp://localhost:61616
                </config-property>
                <connection-definitions>
                    <connection-definition class-name="org.apache.activemq.ra.ActiveMQManagedConnectionFactory" jndi-name="java:jboss/activemq/TopicConnectionFactory" enabled="true" pool-name="TopicConnectionFactory"/>
                    <connection-definition class-name="org.apache.activemq.ra.ActiveMQManagedConnectionFactory" jndi-name="java:jboss/activemq/QueueConnectionFactory" enabled="true" pool-name="QueueConnectionFactory"/>
                </connection-definitions>
                <admin-objects>
                   <admin-object class-name="org.apache.activemq.command.ActiveMQQueue" jndi-name="java:jboss/activemq/queue/TicketQueue" pool-name="TicketQueue">
                       <config-property name="PhysicalName">
                           activemq/queue/TicketQueue
                       </config-property>
                   </admin-object>
               </admin-objects>
           </resource-adapter>
       </resource-adapters>
    </subsystem>

in the 06_Code/resource-adapter/BookingQueueReceiver.java

i changed @ResourceAdapter(value="activemq-5.11.1")

    $ cd 06_Code/
    $ cd resource-adapter/
    $ cp pom.xml ../ticket-agency-jms/
    $ cp BookingQueueReceiver.java ../ticket-agency-jms/src/main/java/com/packtpub/wflydevelopment/chapter6/jms/
    $ cd ../ticket-agency-jms/
    $ mvn package
    $ mvn wildfly:deploy

### Sending JMS by hawtio

hawtio i installed like here:  
http://javadev.org/java_basics/installation/activemq/centos/6/x86_x64/

<div align="center">
    <img src="/files/img/labs/books/packtpub/2014/Java-EE-7-Development-with-WildFly/chapter6_hawtio_pic_01.png" alt="hawtio active mq 5.11">

<br/><br/>

<img src="/files/img/labs/books/packtpub/2014/Java-EE-7-Development-with-WildFly/chapter6_hawtio_pic_02.png" alt="hawtio active mq 5.11">

<br/><br/>

<img src="/files/img/labs/books/packtpub/2014/Java-EE-7-Development-with-WildFly/chapter6_hawtio_pic_03.png" alt="hawtio active mq 5.11">

</div>

the result is:

23:06:58,578 INFO [com.packtpub.wflydevelopment.chapter6.jms.BookingQueueReceiver] (default-threads - 3) Received message This is a message from ActiveMQ!
