---
layout: page
title: Chapter 5. Combining Persistence with CDI
permalink: /labs/books/packtpub/2014/java-ee-7-development-with-wildfly/5/
---

### Setting up the database

    $ psql –U postgres

    CREATE DATABASE ticketsystem;
    CREATE USER jboss WITH PASSWORD ‘jboss’;
    GRANT ALL PRIVILEGES ON	DATABASE ticketsystem TO jboss;

### Installing the JDBC driver in WildFly

    $JBOSS_HOME/modules/org/postgresql/main/postgresql-9.3-1101.jdbc41.jar
    $JBOSS_HOME/modules/org/postgresql/main/module.xml

<br/>
module.xml
<br/>

    <?xml version="1.0" encoding="UTF-8"?>
    <module xmlns="urn:jboss:module:1.0" name="org.postgresql">
        <resources>
            <resource-root path="postgresql-9.4-1201.jdbc41.jar"/>
        </resources>

        <dependencies>
            <module name="javax.api"/>
            <module name="javax.transaction.api"/>
        </dependencies>
    </module>

<br/>
standalone-full.xml
<br/>

    <subsystem xmlns=“urn:jboss:domain:datasources:1.0”>
    <datasources>
        <datasource	jta=“false”
            jndi-name=“java:jboss/datasources/wflydevelopment”
            pool-name=“wflydevelopment”	enabled=“true”>
                <connection-url>
            		jdbc:postgresql://localhost:5432/ticketsystem
            	</connection-url>
            	<driver-class>org.postgresql.Driver</driver-class>
            	<driver>postgresql</driver>
            	<security>
            		<user-name>jboss</user-name>
            		<password>jboss</password>
            	</security>
        </datasource>
        <drivers>
            <driver	name=“postgresql” module=“org.postgresql”/>
        </drivers>
    </datasources>
    </subsystem>

or

    jboss-cli.sh
    connect

<br/>

    /subsystem=datasources/data-source=wflydevelopment:add(jndi-name=java:jboss/datasources/wflydevelopment, driver-name=postgresql, connection-url=jdbc:postgresql://localhost:5432/ticketsystem,user-name=“jboss”,password=“jboss”)

<br/><br/>

However, it is also possible to instruct JPA to generate SQL scripts for you, so you can manually apply them to the database. Simply add the following entries to your persistence-unit tag:

<br/>

    <property name=“javax.persistence.schema-generation-target” value=“scripts”/>
    <property name=“javax.persistence.ddl-create-script-target” value=“createSeats.sql”/>
    <property name=“javax.persistence.ddl-drop-script-target” value=“dropSeats.sql”/>

<br/>

    $ cd 05_Code/ticket-agency-jpa/
    $ mvn clean
    $ mvn package
    $ mvn wildfly:deploy

http://192.168.1.21:8080/ticket-agency-jpa/

<div align="center">
    <img src="/files/img/labs/books/packtpub/2014/Java-EE-7-Development-with-WildFly/chapter5-pic1.png" alt="Java EE 7 Development with WildFly">

<br/><br/>

    <img src="/files/img/labs/books/packtpub/2014/Java-EE-7-Development-with-WildFly/chapter5-pic2.png" alt="Java EE 7 Development with WildFly">

<br/><br/>

    <img src="/files/img/labs/books/packtpub/2014/Java-EE-7-Development-with-WildFly/chapter5-pic3.png" alt="Java EE 7 Development with WildFly">

<br/><br/>

    <img src="/files/img/labs/books/packtpub/2014/Java-EE-7-Development-with-WildFly/chapter5-pic4.png" alt="Java EE 7 Development with WildFly">

</div>
