---
layout: page
title: Chapter 4. Learning Context and Dependency Injection
permalink: /labs/books/packtpub/2014/java-ee-7-development-with-wildfly/4/
---

WTF? Application from this chapter is not starting on Wildfly 8.2.0. But on wersion 8.1.0 all OK! Seems a jsf 2.2 problem

    $ cd 04_Code/ticket-agency-cdi/
    $ mvn clean
    $ mvn package
    $ mvn wildfly:deploy

<!--

    $ mvn wildfly:run

-->

http://192.168.1.21:8080/ticket-agency-cdi/

<div align="center">
    <img src="/files/img/labs/books/packtpub/2014/Java-EE-7-Development-with-WildFly/chapter4-pic1.png" alt="Java EE 7 Development with WildFly">
</div>
