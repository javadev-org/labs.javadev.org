---
layout: page
title: Java EE 7 Development with WildFly
permalink: /labs/books/packtpub/2014/java-ee-7-development-with-wildfly/
---

<div align="center">
    <img src="/files/img/labs/books/packtpub/2014/Java-EE-7-Development-with-WildFly/Java_EE_7_Development_with_WildFly_Cover.jpg" alt="Java EE 7 Development with WildFly">
</div>

<br/><br/>

SRC:  
https://github.com/javalabs-org/Java-EE-7-Development-with-WildFly

---

We will use:

<ul>

    <li>VirtualBox</li>
    <li>Centos 6.6</li>
    <li><a href="//javadev.org/development-tools/jdk/installation/centos/7/">JDK 1.8</a></li>
    <li><a href="//javadev.org/development-tools/assembly-tools/maven/linux/centos/7/">Maven 3.3.1</a></li>
    <li><a href="//javadev.org/docs/appserv/wildfly/8.2/installation/">Wildfly 8.1</a> (There was problem with 8.2)</li>
    <li>PostgreSQL 9.X</li>
    <li>ActiveMQ</li>

</ul>

<br/><br/>

To start wildfly server:

    $ standalone.sh -c standalone-full.xml -b=0.0.0.0 -bmanagement=0.0.0.0

<br/><br/>

    $ cd ~
    $ mkdir projects
    $ cd projects/
    $ git clone https://github.com/javalabs-org/Java-EE-7-Development-with-WildFly
    $ cd Java-EE-7-Development-with-WildFly/

---

Table of Contents:

<ul>

    <li><a href="/labs/books/packtpub/2014/java-ee-7-development-with-wildfly/3/">Chapter 3. Introducing Java EE 7 – EJBs</a></li>

    <li><a href="/labs/books/packtpub/2014/java-ee-7-development-with-wildfly/4/">Chapter 4. Learning Context and Dependency Injection</a></li>

    <li><a href="/labs/books/packtpub/2014/java-ee-7-development-with-wildfly/5/">Chapter 5. Combining Persistence with CDI</a></li>

    <li><a href="/labs/books/packtpub/2014/java-ee-7-development-with-wildfly/6/">Chapter 6. Developing Applications with JBoss JMS Provider</a></li>

</ul>
