---
layout: page
title: Chapter 3. Introducing Java EE 7 – EJBs
permalink: /labs/books/packtpub/2014/java-ee-7-development-with-wildfly/3/
---


    $ cd 03_Code/ticket-agency/
    $ mvn install


<br/>

    $ cd ticket-agency-ejb
    $ mvn wildfly:deploy

<br/>

    INFO: JBoss Remoting version 4.0.3.Final
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 27.915 s
    [INFO] Finished at: 2015-05-12T20:21:15-04:00
    [INFO] Final Memory: 15M/37M
    [INFO] ------------------------------------------------------------------------

<br/>

    $ jboss-cli.sh
    You are disconnected at the moment. Type 'connect' to connect to the server or 'help' for the list of supported commands.

    [disconnected /] connect

    [standalone@localhost:9990 /] deploy
    ticket-agency-ejb.jar

    [standalone@localhost:9990 /] exit


<br/>

    $ cd ../ticket-agency-ejb-client/
    $ mvn package exec:exec


<br/>

    Theatre booking system
    =====================================
    Commands: book, bookasync, list, mail, money, quit

    > bookasync

    Enter SeatId: 5
    May 12, 2015 8:36:45 PM com.packtpub.wflydevelopment.chapter3.client.TicketAgencyClient handleBookAsync
    INFO: Booking issued. Verify your mail!


==================
==================

TO run application on Eclipse:


    RUN -> RUN Configuration -> Maven Build

    Name:
    ticket-agency-ejb deploy

    Base directory:
    ${workspace_loc:/ticket-agency-ejb}

    Goals:
    wildfly:deploy


    =======================================


    Name:
    ticket-agency-ejb-client

    Base directory:
    ${workspace_loc:/ticket-agency-ejb-client}

    Goals:
    install exec:exec



<!--

Tasks:


Deploying your first application to	WildFly	8

Deploying	applications	using	the	command-line	interface



cd wildfly-8.2.0.Final/bin

$ ./jboss-cli.sh

[disconnected /] connect

[standalone@localhost:9990 /] deploy
HelloWorld.war


[standalone@localhost:9990 /] deploy ../HelloWorld.war

If	you	want	to	just	perform	the deployment	of	the	application	and	defer	the	activation	to	a	later	time
[standalone@localhost:9990 /] deploy ../HelloWorld.war —disabled


In	order	to	activate	the	application
[standalone@localhost:9990 /] deploy -–name=HelloWorld.war


Redeploying	the	application

[localhost:9990	/] deploy -f	../HelloWorld.war

-f - force


Undeploying	the	application
[localhost:9990	/] undeploy	HelloWorld.war



### Chapter	3.	Introducing	Java EE	7 –	EJBs


Developing	singleton	EJBs


We created new Maven Project
Then created new Maven Module


Configuring	the	EJB	project	object	module	(pom.xml)


-->
