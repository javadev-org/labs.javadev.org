---
layout: page
title: Lab 7 Java 8 Lambda Basics
permalink: /labs/lab-07/
---

# [YouTube, JavaBrains] Java 8 Lambda Basics

<div align="center">

    <iframe width="853" height="480" src="https://www.youtube.com/embed/videoseries?list=PLqq-6Pq4lTTa9YGfyhyW2CqdtW9RtY-I3" frameborder="0" allowfullscreen></iframe>

</div>

<br/>
<br/>

<a href="https://bitbucket.org/javadev-org/java_8_lambda_basics/src/master/">My SRC</a>

<br/>
<br/>

UNIT 01  
Using Lambdas - This unit is an introduction to lambdas in Java 8. Learn what lambdas are, the syntax and how to use them. Compare them with anonymous inner classes. Understand functional interfaces and use lambdas to work with legacy interfaces like Runnable.

    Introduction
    Why Lambdas
    Dev setup
    Functional vs Object Oriented Programming
    Passing Behavior in OOP
    Introducing Lambda Expressions
    Lambda Expression Examples
    Lambda as interface type
    Lambdas vs Interface Implementations
    Type Inference
    Runnable Using Lambdas
    Functional Interface
    Lambda Exercise
    Lambda Exercise Solution

<br/>
<br/>

### Java 8 Lambda Basics 9 - Lambdas vs Interface Implementations

<br/><br/>

**Greeting.java**

<br/>

{% highlight java linenos %}

package org.javalabs;

public interface Greeting {

    public void perform();

}

{% endhighlight %}

<br/><br/>

**Greeter.java**

<br/>

{% highlight java linenos %}

package org.javalabs;

public class Greeter {

    public  void greet(Greeting greeting){
    	greeting.perform();
    }

    public static void main(String[] args){

    	Greeter greeter = new Greeter();

    	Greeting lambdaGreeting = () -> System.out.println("Hello world!");

    	Greeting innerClassGreeting  = new Greeting(){
    		public void perform(){
    			System.out.println("Hello world!");
    		}
    	};

// lambdaGreeting.perform();
// innerClassGreeting.perform();

    	greeter.greet(lambdaGreeting);
    	greeter.greet(innerClassGreeting);

    }

} // The End of Class

{% endhighlight %}
