---
layout: page
title: Site Map
permalink: /sitemap/
---

# Site Map

<br/>

**Books / Magazines:**

<ul>
    <li><a href="/library/magazines/java_magazine/">List of free java magazines</a></li>
    <li><a href="/library/books/classic/free/">Java Free eBooks Collection</a></li>
    <li><a href="/library/books/classic/">Java Paid Classic Book Collection</a></li>
</ul>

<br/>

**Tutorials:**

<ul>
    <li><a href="http://www.guru99.com/java-platform.html">Java tutorials (guru99)</a></li>
</ul>

<br/>

**Video Courses:**

<ul>
    <li><a href="/library/collection/video/java/list/free/">List of free java video courses</a></li>
    <li><a href="/library/collection/video/java/list/paid/">List of paid java video courses</a></li>
</ul>

<br/>

**Conferences:**

<ul>
    <li><a href="/conferences/">Java Conferences</a></li>
</ul>

<br/>

**Code Examples:**

<ul>
    <li><a href="https://javadev.org/projects/">Code Examples</a></li>
</ul>

<br/>

**Some Videos About Java Development From YouTube:**

<ul>
    <li><a href="/youtube-videos/">Here</a></li>
</ul>
